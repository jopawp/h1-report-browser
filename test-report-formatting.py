#!/usr/bin/env python2

import os
import sys
import subprocess

mypath = "./reports"
DEVNULL = open(os.devnull, 'w')
onlyfiles = [f for f in os.listdir(mypath) if os.path.isfile(os.path.join(mypath, f))]
for f in onlyfiles:
	report = f.split(".")[0]
	r = subprocess.call(["python2", "./h1-format-report.py", mypath + "/" + report], stdout=DEVNULL, stderr=subprocess.STDOUT)
	if r != 0:
		print("FAIL: " + report)
