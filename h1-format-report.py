#!/usr/bin/env python
## -*- coding: utf-8 -*-
# 
# File: h1-format-report.py
# Author: Matthew A. Weidner <@jopawp>
# Date: 09/04/2016
# Descr: Formats an arbitary HackerOne report from offline cache created by
# 		 h1-dl-reports.py. Output is colorized. Report format may vary from
# 		 h1-browser.py.

import sys
import json
import pdb
import pprint

G = '\033[92m' #green
Y = '\033[93m' #yellow
B = '\033[94m' #blue
R = '\033[91m' #red
W = '\033[0m'  #white

def split_lines(maxlen, line):
	r = []
	#line = line.replace('\n', '')
	#' '.join(line.split())
	while len(line) > maxlen:
		spaces = []
		for i, letter in enumerate(line):
			if letter == ' ':
				#print(i, letter)
				spaces.append(i)
		#spaces = [i for i, letter in enumerate(line) if letter == ' ']
		split_pos = [i for i, space in enumerate(spaces) if space <= maxlen]
		#print(i)
		#print(len(spaces))
		#print(len(split_pos))
		#print(spaces)
		#print(split_pos)
		#pdb.set_trace()
		#input(">> ")
		if len(split_pos) > 0:
			if spaces[split_pos[-1]] == 0:
				str=line[0:maxlen]
				spaces[split_pos[-1]] = maxlen
			else:
				str=line[0:spaces[split_pos[-1]]]
		else:
			str = ""
		t = str.strip()
		#print('line: ', t)
		if t is not '':
			r.append([str.strip()])
		if len(split_pos) > 0:
			line=line[spaces[split_pos[-1]]:]
		else:
			line = ""
		#input(">> ")
	if len(line) > 0:
		r.append([line.strip()])

	#for s in r:
	#	print(s[0])

if __name__ == '__main__':
	if len(sys.argv) < 2:
		print('Usage:', sys.argv[0], '<report_number>')
		exit()
	if sys.argv[1][-5:] == '.json':
		f = open(sys.argv[1])
	else:
		f = open(sys.argv[1] + '.json')
	j = json.loads(f.read())
	report_id = j['id']
	try:
		if j['reporter'] is not None:
			reporter = j['reporter']['username']
		else:
			reporter = 'Unknown'
			raise ValueError('Reporter field is None.')
	except:
		reporter = 'Unknown'
	url = j['url']
	created_at = j['created_at']
	title = j['title']
	team_id = j['team']['id']
	team = j['team']['profile']['name']
	try:
		vuln_types = j['vulnerability_types']
	except:
		try:
			vuln_types = [j['weakness']]
		except:
			vuln_types = [{'id':9999, 'name':'Unknown' }]
	print(vuln_types)
	report = j['vulnerability_information']
	if j['has_bounty?'] == True:
		try:
			bounty = j['formatted_bounty']
		except:
			bounty = 'Has bounty, but unable to determine amount'
	else:
		bounty = "No bounty."

	print(Y + 'Bonty Program: (' + repr(team_id) + ')', team + W)
	print(G + 'Date:' + W, created_at)
	print(G + 'Report url:' + W, url)
	print(G + 'Vulnerability Types:' + W)
	for vuln_type in vuln_types:
		print(R + '  ', '(' + repr(vuln_type['id']) + ')', vuln_type['name'] + W)
	print()
	print(Y + 'Bounty: ' + G + bounty + W)
	print()
	print(G + 'Report by <' + Y + reporter + G + '> on ' + W + created_at)
	print()
	split_lines(79, report)
	activities = j['activities']
	print('\n\n' + G + 'Comments:' + W)
	for i, a in enumerate(activities):
		activity_type = a['type'][12:]
		try:
			m = a['message']
		except:
			m = None
		if m is None:
			continue
		if len(m) > 0:
			if a['actor'] is None:
				user = 'Unknown'
			else:
				try:
					user = a['actor']['username']
				except:
					try:
						user = a['actor']['profile']['name']
					except:
						user = 'UNKNOWN'
			print(B + '(Event ' + repr(i) + ')', activity_type, '-'*(40-len(activity_type)) + W + '\n' \
					+ Y + '  <' + user + '>' + W \
					+ '  ' + a['created_at'] + '\n' \
					+ '    ' + m)


