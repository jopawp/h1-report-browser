# H1 Report Browser
## Author: Matthew A. Weidner

* h1-browser.py:
Ncurses terminal based HackerOne report browser. Uses an offline cache of HackerOne reports stored in a SQLite3 database.

* h1-dl-reports.py:
Utilizes h1.nobbd.de public web service to download newle published reports from HackerOne. Stages raw json reports for merging into database.

* h1-format-report.py:
Simple one-shot HackerOne report formatter of the raw json report.

* h1_stats.py:
Calculates bounty statistics from HackerOne reports.

* import_new_reports.py:
Imports new reports from staging area into SQLite database.

* import_reports.py:
Imports all reports from staging area into SQLite database.
